package gui;


import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class WindowState implements Serializable{
    public Point location;
    public Dimension size;
    public String type;

    WindowState(JInternalFrame frame) {
        type = frame.getTitle();
        location = frame.getLocation();
        size = frame.getSize();
    }
}
