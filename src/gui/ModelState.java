package gui;


import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

public class ModelState implements Serializable{
    public double robotPositionX = 100;
    public double robotPositionY = 100;
    public double robotDirection = 0;

    public ArrayList<Rectangle> obstacles;

    public ModelState(GameModel model)
    {
        double[] robotPosition = model.getRobotPosition();
        robotPositionX = robotPosition[0];
        robotPositionY = robotPosition[1];
        robotDirection = model.getRobotDirection();

        obstacles = model.getM_obstacles();
    }
}
