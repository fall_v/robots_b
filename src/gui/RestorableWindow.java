package gui;


import java.io.FileNotFoundException;
import java.io.IOException;

public interface RestorableWindow {
    void saveState() throws IOException;
    void restoreState() throws Exception;
}
