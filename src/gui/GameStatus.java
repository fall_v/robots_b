package gui;


import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class GameStatus extends JInternalFrame implements Observer {
    GameModel m_model;
    JTextField m_coordinatesField;

    public GameStatus(GameModel model){
        super("Координаты робота", false, false, false, false);
        m_model = model;
        m_model.addObserver(this);

        JPanel panel = new JPanel(new BorderLayout());
        setPreferredSize(new Dimension(200, 100));

        double[] robotPos = m_model.getRobotPosition();

        String posX = String.format("%.2f",robotPos[0]);
        String posY = String.format("%.2f",robotPos[1]);

        m_coordinatesField = new JTextField(posX + " " + posY);
        m_coordinatesField.setEditable(false);
        m_coordinatesField.setPreferredSize(new Dimension(200, 50));

        panel.add(m_coordinatesField, BorderLayout.SOUTH);
        getContentPane().add(panel);
        pack();
    }

    private void updateCoordinates(double[] pos) {
        try {
            String posX = String.format("%.2f",pos[0]);
            String posY = String.format("%.2f",pos[1]);
            m_coordinatesField.setText(posX + " " + posY);
        }
        catch (NullPointerException e) {}
    }

    @Override
    public void update(Observable o, Object arg) {
        double[] pos = m_model.getRobotPosition();
        updateCoordinates(pos);
    }
}
