package gui;

import java.util.*;
import java.awt.*;
import java.util.List;


class Node {
    public Point location;
    public double g;
    public double h;
    public Node parent;
    public boolean isWall = false;
    private final int MOVEMENT_COST = 10;

    public Node(Point point) {
        location = point;
    }

    public void setGValue(int amount) {
        this.g = amount;
    }

    public void calculateHValue(Node destPoint) {
        this.h = (Math.abs(location.x - destPoint.location.x) + Math.abs(location.y - destPoint.location.y)) * this.MOVEMENT_COST;
    }

    public void calculateGValue(Node point) {
        this.g = point.g + this.MOVEMENT_COST;
    }

    public double getFValue() {
        return this.g + this.h;
    }
}

public class AStar {
    private final int WIDTH;
    private final int HEIGHT;

    private final Map<Point, Node> nodes = new HashMap<Point, Node>();

    private final Comparator fComparator = new Comparator<Node>() {
        public int compare(Node a, Node b) {
            return Double.compare(a.getFValue(), b.getFValue());
        }
    };

    public AStar(int width, int height, ArrayList<Point> walls) {
        WIDTH = width ;
        HEIGHT = height ;

        for (int x = 0; x <= WIDTH; x++) {
            for (int y = 0; y <= HEIGHT; y++) {
                Point point = new Point(x, y);
                this.nodes.put(point, new Node(point));
            }
        }
        for (Point point : walls) {
            Node node = this.nodes.get(point);

            node.isWall = true;
        }
    }


    public Stack<Point> calculateAStarNoTerrain(Point p1, Point p2) {

        List<Node> openList = new ArrayList<>();
        List<Node> closedList = new ArrayList<>();

        Node destNode = this.nodes.get(p2);

        Node currentNode = this.nodes.get(p1);

        currentNode.parent = null;
        currentNode.setGValue(0);
        openList.add(currentNode);

        int con = 1;
        while(!openList.isEmpty()) {
            Collections.sort(openList, this.fComparator);
            currentNode = openList.get(0);

            if (currentNode.location.equals(destNode.location)) {
                return this.calculatePath(destNode);
            }

            openList.remove(currentNode);
            closedList.add(currentNode);

            ArrayList<Point> neighbours = getNeighbours(currentNode.location);

            for (Point adjPoint : neighbours) {

                if (!this.isInsideBounds(adjPoint)) {
                    continue;
                }

                Node adjNode = this.nodes.get(adjPoint);
                if (adjNode.isWall) {
                    continue;
                }

                if (!closedList.contains(adjNode)) {
                    if (!openList.contains(adjNode)) {
                        adjNode.parent = currentNode;
                        adjNode.calculateGValue(currentNode);
                        adjNode.calculateHValue(destNode);
                        openList.add(adjNode);
                    } else {
                        if (adjNode.g < currentNode.g) {
                            adjNode.calculateGValue(currentNode);
                            currentNode = adjNode;
                        }
                    }
                }
            }
        }
        return new Stack<>();
    }

    private boolean isInsideBounds(Point p) {
        return p.x >= 0 && p.x < WIDTH && p.y >= 0 && p.y < HEIGHT;
    }

    private ArrayList<Point> getNeighbours(Point p) {
        ArrayList<Point> neighbours = new ArrayList<>();
        for (int i = -1; i <= 1; i++)
            for (int j = -1; j <= 1; j++)
            {
                if (i == 0 && j == 0)
                    continue;
                neighbours.add(new Point(p.x + i, p.y + j));
            }
        return neighbours;
    }

    private Stack<Point> calculatePath(Node destinationNode) {
        Stack<Point> path = new Stack<>();
        Node node = destinationNode;
        while (node.parent != null) {
            path.push(node.location);
            node = node.parent;
        }
        return path;
    }
}
