package gui;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.io.*;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;
import javax.swing.plaf.basic.BasicScrollPaneUI;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import log.Logger;
import sun.applet.Main;

/**
 * Что требуется сделать:
 * 1. Метод создания меню перегружен функционалом и трудно читается. 
 * Следует разделить его на серию более простых методов (или вообще выделить отдельный класс).
 *
 */
public class MainApplicationFrame extends JFrame implements RestorableWindow
{
    private final JDesktopPane desktopPane = new JDesktopPane();
    public GameModel m_model = new GameModel();

    public MainApplicationFrame() throws Exception {
        //Make the big window be indented 50 pixels from each edge
        //of the screen.

        int inset = 50;
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(inset, inset,
                screenSize.width  - inset*2,
                screenSize.height - inset*2);
        setContentPane(desktopPane);
        File f = new File(System.getProperty("user.home") + "\\robots_config.bin");
        if(!f.exists())
        {
            MenuGenerator menuGenerator = new MenuGenerator(this, desktopPane);
            menuGenerator.generateStandartWindows();
            m_model.generateStandartObstacles();
        }
        else {
            restoreState();
        }
        setJMenuBar(generateMenuBar());

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                try {
                    close(e);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    private void close(WindowEvent e) throws IOException {
        String[] options = {"Да", "Нет"};
        int n = JOptionPane
                .showOptionDialog(e.getWindow(), "Закрыть окно?",
                        "Подтверждение", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options,
                        options[0]);
        if (n == 0) {
            saveState();
            e.getWindow().setVisible(false);
            System.exit(0);
        }
    }

    private JMenuBar generateMenuBar()
    {
        JMenuBar menuBar = new JMenuBar();
        MenuGenerator menuGenerator = new MenuGenerator(this, desktopPane);

        menuBar.add(menuGenerator.generateNewWindowsMenu());
        menuBar.add(menuGenerator.generateLookAndFeelMenu());
        menuBar.add(menuGenerator.generateTestMenu());
        menuBar.add(menuGenerator.generateCloseItem());
        return menuBar;
    }

    @Override
    public void saveState() throws IOException {
        Object[] states = new Object[2];
        ModelState modelState = new ModelState(m_model);
        ArrayList<WindowState> frameStates = new ArrayList<>();
        for (JInternalFrame frame : desktopPane.getAllFrames())
            frameStates.add(new WindowState(frame));
        states[0] = modelState;
        states[1] = frameStates;
        FileOutputStream fos = new FileOutputStream(System.getProperty("user.home") + "\\robots_config.bin");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(states);
        oos.flush();
        oos.close();
    }

    @Override
    public void restoreState() throws Exception {
        try {
            FileInputStream fis = new FileInputStream(System.getProperty("user.home") + "\\robots_config.bin");
            ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(fis));
            Object[] states = (Object[]) ois.readObject();
            ModelState modelState = (ModelState) states[0];
            m_model.restoreState(modelState);
            ArrayList<WindowState> restored = (ArrayList<WindowState>) states[1];
            MenuGenerator menuGenerator = new MenuGenerator(this, desktopPane);
            for (WindowState frameState : restored) {
                menuGenerator.createFrameFromState(frameState);
            }
            ois.close();
        }
        catch (ClassCastException e1)
        {
            try
            {
                ArrayList<WindowState> frameStates = new ArrayList<>();
                for (JInternalFrame frame : desktopPane.getAllFrames())
                    frameStates.add(new WindowState(frame));
                FileOutputStream fos = new FileOutputStream(System.getProperty("user.home") + "\\robots_config.bin");
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(frameStates);
                oos.flush();
                oos.close();
            }
            catch (ClassCastException e2)
            {
                MenuGenerator menuGenerator = new MenuGenerator(this, desktopPane);
                menuGenerator.generateStandartWindows();
                m_model.generateStandartObstacles();
            }
        }
    }
}
