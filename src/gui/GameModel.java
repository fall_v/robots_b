package gui;


import java.awt.*;
import java.util.*;

public class GameModel extends Observable {
    private volatile double m_robotPositionX = 100;
    private volatile double m_robotPositionY = 100;
    private volatile double m_robotDirection = 0;

    private volatile int m_targetPositionX = 150;
    private volatile int m_targetPositionY = 100;

    private static final double maxVelocity = 0.1;
    private static final double maxAngularVelocity = 0.005;

    private int maxWidth = 1000;
    private int maxHeight = 1000;
    private int cellSize = 10;

    private ArrayList<Rectangle> m_obstacles = new ArrayList<>();
    private Stack<Point> path = new Stack<>();
    private AStar pathFinder;

    private volatile Point intermediate;
    private boolean movingToIntermediate = false;

    private final Timer m_timer = initTimer();

    public GameModel() {
        m_timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                onModelUpdateEvent();
            }
        }, 0, 10);
        m_obstacles.add(new Rectangle(1,1,40,40));
        m_obstacles.add(new Rectangle(40,1,40,40));
        m_obstacles.add(new Rectangle(200,200, 100, 100));
        ArrayList<Point> walls = getWalls(m_obstacles);
        pathFinder = new AStar(maxWidth / cellSize, maxHeight / cellSize, walls);
    }

    public void restoreState(ModelState modelState) {
        m_robotPositionX = modelState.robotPositionX;
        m_robotPositionY = modelState.robotPositionY;
        m_robotDirection = modelState.robotDirection;
        m_obstacles = modelState.obstacles;
    }

    public void generateStandartObstacles() {
        m_obstacles.add(new Rectangle(1,1,40,40));
        m_obstacles.add(new Rectangle(40,1,40,40));
        m_obstacles.add(new Rectangle(200,200, 100, 100));
    }

    private static Timer initTimer()
    {
        Timer timer = new Timer("events generator", true);
        return timer;
    }



    public double[] getRobotPosition() {
        return new double[]{m_robotPositionX, m_robotPositionY};
    }

    public int[] getTargetPosition() {
        return new int[]{m_targetPositionX, m_targetPositionY};
    }

    public double getRobotDirection() {
        return m_robotDirection;
    }

    private ArrayList<Point> getWalls(ArrayList<Rectangle> obstacles)
    {
        ArrayList<Point> result = new ArrayList<>();
        for (int row = 0; row <= maxWidth / cellSize; row++)
            for (int col = 0; col <= maxHeight / cellSize; col++)
            {
                Rectangle current = new Rectangle(row * cellSize, col * cellSize, cellSize, cellSize);
                for (Rectangle obstacle : obstacles)
                {
                    if (current.intersects(obstacle)) {
                        result.add(new Point(row, col));
                        break;
                    }
                }
            }
        return result;
    }


    protected void setTargetPosition(Point p)
    {
        path.clear();
        m_targetPositionX = p.x;
        m_targetPositionY = p.y;
        path = pathFinder.calculateAStarNoTerrain(new Point((int)m_robotPositionX / cellSize, (int)m_robotPositionY / cellSize),
                new Point(m_targetPositionX / cellSize, m_targetPositionY / cellSize));

        movingToIntermediate = false;

    }

    private static double distance(double x1, double y1, double x2, double y2)
    {
        double diffX = x1 - x2;
        double diffY = y1 - y2;
        return Math.sqrt(diffX * diffX + diffY * diffY);
    }

    private static double angleTo(double fromX, double fromY, double toX, double toY)
    {
        double diffX = toX - fromX;
        double diffY = toY - fromY;

        return asNormalizedRadians(Math.atan2(diffY, diffX));
    }
    public ArrayList<Rectangle> getM_obstacles(){
        return m_obstacles;
    }
    private static double applyLimits(double value, double min, double max)
    {
        if (value < min)
            return min;
        if (value > max)
            return max;
        return value;
    }

    protected void onModelUpdateEvent()
    {
        if (movingToIntermediate) {
            moveTo(intermediate.x, intermediate.y);
            return;
        }
        if (!path.isEmpty())
        {
            Point temp = path.pop();
            intermediate = new Point(temp.x * cellSize + cellSize / 2, temp.y * cellSize + cellSize / 2);
            movingToIntermediate = true;
        }

    }
    private void moveTo(int targetX, int targetY)
    {
        double distance = distance(targetX, targetY, m_robotPositionX, m_robotPositionY);
        if (distance < 0.5)
        {
            movingToIntermediate = false;
            return;
        }
        double velocity = maxVelocity;
        double angleToTarget = angleTo(m_robotPositionX, m_robotPositionY, targetX, targetY);
        double angularVelocity = 0;
        if (isAngleGreater(m_robotDirection, angleToTarget))
        {
            angularVelocity = maxAngularVelocity;
        }
        else {
            angularVelocity = -maxAngularVelocity;
        }
        moveRobot(velocity, angularVelocity, 10, targetX, targetY);
    }

    private void moveRobot(double velocity, double angularVelocity, double duration, int targetX, int targetY)
    {
        velocity = applyLimits(velocity, 0, maxVelocity);
        angularVelocity = applyLimits(angularVelocity, -maxAngularVelocity, maxAngularVelocity);
        if (angleDistance(m_robotDirection, angleTo(m_robotPositionX, m_robotPositionY, targetX, targetY)) < angularVelocity * 10) {

            double newX = m_robotPositionX + velocity / angularVelocity *
                    (Math.sin(m_robotDirection + angularVelocity * duration) -
                            Math.sin(m_robotDirection));
            if (!Double.isFinite(newX)) {
                newX = m_robotPositionX + velocity * duration * Math.cos(m_robotDirection);
            }
            double newY = m_robotPositionY - velocity / angularVelocity *
                    (Math.cos(m_robotDirection + angularVelocity * duration) -
                            Math.cos(m_robotDirection));
            if (!Double.isFinite(newY)) {
                newY = m_robotPositionY + velocity * duration * Math.sin(m_robotDirection);
            }
            m_robotPositionX = newX;
            m_robotPositionY = newY;
        }
        double newDirection = asNormalizedRadians(m_robotDirection + angularVelocity * duration);
        m_robotDirection = newDirection;
        setChanged();
        notifyObservers();
        clearChanged();
    }

    private static boolean isAngleGreater(double robotDirection, double angleTo)
    {
        double r1, r2;
        if (robotDirection > angleTo) {
            r1 = robotDirection - angleTo;
            r2 = angleTo - robotDirection + 2 * Math.PI;
            return r2 < r1;
        }
        else {
            r1 = angleTo - robotDirection;
            r2 = robotDirection - angleTo + 2 * Math.PI;
            return r1 < r2;
        }
    }

    private static double angleDistance(double robotDirection, double angleTo)
    {
        double r1, r2;
        if (robotDirection > angleTo) {
            r1 = robotDirection - angleTo;
            r2 = angleTo - robotDirection + 2 * Math.PI;
        }
        else {
            r1 = angleTo - robotDirection;
            r2 = robotDirection - angleTo + 2 * Math.PI;
        }
        return (r1 > r2) ? r2 : r1;
    }

    private static double asNormalizedRadians(double angle)
    {
        while (angle < 0)
        {
            angle += 2*Math.PI;
        }
        while (angle >= 2*Math.PI)
        {
            angle -= 2*Math.PI;
        }
        return angle;
    }
}
