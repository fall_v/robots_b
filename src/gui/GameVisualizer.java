package gui;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

public class GameVisualizer extends JPanel implements Observer
{
    GameModel m_model;

    public GameVisualizer(GameModel model)
    {
        m_model = model;
        m_model.addObserver(this);

        addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                m_model.setTargetPosition(e.getPoint());
            }
        });
        setDoubleBuffered(true);
    }

    protected void onRedrawEvent()
    {
        EventQueue.invokeLater(this::repaint);
    }

    private static int round(double value)
    {
        return (int)(value + 0.5);
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        Graphics2D g2d = (Graphics2D)g;
        double[] robotPosition = m_model.getRobotPosition();
        double robotDir = m_model.getRobotDirection();
        int[] targetPos = m_model.getTargetPosition();
        drawRobot(g2d, round(robotPosition[0]), round(robotPosition[1]), robotDir);
        drawTarget(g2d, targetPos[0], targetPos[1]);
        for (Rectangle obst : m_model.getM_obstacles())
        {
            drawRectangle(g2d, obst.x, obst.y, obst.width, obst.height);
        }
    }

    private static void fillOval(Graphics g, int centerX, int centerY, int diam1, int diam2)
    {
        g.fillOval(centerX - diam1 / 2, centerY - diam2 / 2, diam1, diam2);
    }

    private static void drawOval(Graphics g, int centerX, int centerY, int diam1, int diam2)
    {
        g.drawOval(centerX - diam1 / 2, centerY - diam2 / 2, diam1, diam2);
    }

    private void drawRobot(Graphics2D g, int x, int y, double direction)
    {
        double[] targetPos = m_model.getRobotPosition();
        int robotCenterX = round(targetPos[0]);
        int robotCenterY = round(targetPos[1]);
        AffineTransform t = AffineTransform.getRotateInstance(direction, robotCenterX, robotCenterY);
        g.setTransform(t);
        g.setColor(Color.MAGENTA);
        fillOval(g, robotCenterX, robotCenterY, 30, 10);
        g.setColor(Color.BLACK);
        drawOval(g, robotCenterX, robotCenterY, 30, 10);
        g.setColor(Color.WHITE);
        fillOval(g, robotCenterX  + 10, robotCenterY, 5, 5);
        g.setColor(Color.BLACK);
        drawOval(g, robotCenterX  + 10, robotCenterY, 5, 5);
    }

    private void drawTarget(Graphics2D g, int x, int y)
    {
        AffineTransform t = AffineTransform.getRotateInstance(0, 0, 0);
        g.setTransform(t);
        g.setColor(Color.GREEN);
        fillOval(g, x, y, 5, 5);
        g.setColor(Color.BLACK);
        drawOval(g, x, y, 5, 5);
    }

    private void drawRectangle(Graphics2D g, int x, int y, int width, int height)
    {
        AffineTransform t = AffineTransform.getRotateInstance(0, 0, 0);
        g.setTransform(t);
        g.setColor(Color.black);
        g.fillRect(x,y,width,height);
        g.drawRect(x,y,width,height);
    }

    @Override
    public void update(Observable o, Object arg) {
        onRedrawEvent();
    }
}
