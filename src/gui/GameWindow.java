package gui;

import java.awt.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;
import javax.swing.border.BevelBorder;

public class GameWindow extends JInternalFrame {
    private final GameVisualizer m_visualizer;
    private final GameModel m_model;

    public GameWindow(GameModel model)
    {
        super("Игровое поле", true, true, true, true);
        m_model = model;
        m_visualizer = new GameVisualizer(m_model);
        JPanel panel = new JPanel(new BorderLayout());

        panel.add(m_visualizer, BorderLayout.CENTER);
        getContentPane().add(panel);
        pack();
    }
}
