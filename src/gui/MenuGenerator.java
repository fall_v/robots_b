package gui;


import log.Logger;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;


public class MenuGenerator {
    MainApplicationFrame m_mainFrame;
    JDesktopPane m_desktopPane;
    GameModel m_model;

    MenuGenerator(MainApplicationFrame frame, JDesktopPane desktopPane) {
        m_mainFrame = frame;
        m_model = frame.m_model;
        m_desktopPane = desktopPane;
    }

    protected LogWindow createLogWindow()
    {
        LogWindow logWindow = new LogWindow(Logger.getDefaultLogSource());
        logWindow.setLocation(10,10);
        logWindow.setSize(300, 800);
        m_mainFrame.setMinimumSize(logWindow.getSize());
        logWindow.pack();
        Logger.debug("Протокол работает");
        return logWindow;
    }

    public void generateStandartWindows() {
        GameWindow gameWindow = new GameWindow(m_model);
        gameWindow.setSize(400,  400);
        gameWindow.setLocation(50,50);
        LogWindow logWindow = createLogWindow();
        GameStatus status = new GameStatus(m_model);
        addWindow(status);
        addWindow(gameWindow);
        addWindow(logWindow);
    }

    JMenu generateLookAndFeelMenu()
    {
        JMenu lookAndFeelMenu = new JMenu("Режим отображения");
        lookAndFeelMenu.setMnemonic(KeyEvent.VK_V);
        lookAndFeelMenu.getAccessibleContext().setAccessibleDescription(
                "Управление режимом отображения приложения");

        {
            JMenuItem systemLookAndFeel = new JMenuItem("Системная схема", KeyEvent.VK_S);
            systemLookAndFeel.addActionListener((event) -> {
                setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                m_mainFrame.invalidate();
            });
            lookAndFeelMenu.add(systemLookAndFeel);
        }

        {
            JMenuItem crossplatformLookAndFeel = new JMenuItem("Универсальная схема", KeyEvent.VK_S);
            crossplatformLookAndFeel.addActionListener((event) -> {
                setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
                m_mainFrame.invalidate();
            });
            lookAndFeelMenu.add(crossplatformLookAndFeel);
        }
        return lookAndFeelMenu;
    }

    JMenu generateTestMenu()
    {
        JMenu testMenu = new JMenu("Тесты");
        testMenu.setMnemonic(KeyEvent.VK_T);
        testMenu.getAccessibleContext().setAccessibleDescription(
                "Тестовые команды");

        {
            JMenuItem addLogMessageItem = new JMenuItem("Сообщение в лог", KeyEvent.VK_S);
            addLogMessageItem.addActionListener((event) -> {
                Logger.debug("Новая строка");
            });
            testMenu.add(addLogMessageItem);
        }
        return testMenu;
    }

    JMenu generateNewWindowsMenu()
    {
        JMenu newWindowsMenu = new JMenu("Новое окно");
        newWindowsMenu.getAccessibleContext().setAccessibleDescription(
                "Создание новых окон");

        {
            JMenuItem createGameWindow = new JMenuItem("Игровое окно");
            createGameWindow.addActionListener((event) -> {
                GameWindow gameWindow = new GameWindow(m_model);
                gameWindow.setSize(400,  400);
                addWindow(gameWindow);
            });
            newWindowsMenu.add(createGameWindow);
        }

        {
            JMenuItem createLogWindow = new JMenuItem("Окно логов");
            createLogWindow.addActionListener((event) -> {
                LogWindow logWindow = createLogWindow();
                addWindow(logWindow);
            });
            newWindowsMenu.add(createLogWindow);
        }
        return newWindowsMenu;
    }

    JMenuItem generateCloseItem()
    {
        JMenuItem closingItem = new JMenuItem("Выход");
        closingItem.addActionListener((event) -> {
            Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(
                    new WindowEvent(m_mainFrame, WindowEvent.WINDOW_CLOSING));
        });
        return closingItem;
    }

    public void createFrameFromState(WindowState state) throws Exception {
        JInternalFrame frame;
        switch (state.type){
            case "Протокол работы":
                frame = createLogWindow();
                frame.setLocation(state.location);
                frame.setSize(state.size);
                addWindow(frame);
                break;
            case "Игровое поле":
                frame = new GameWindow(m_model);
                frame.setLocation(state.location);
                frame.setSize(state.size);
                addWindow(frame);
                break;
            case "Координаты робота":
                frame = new GameStatus(m_model);
                frame.setLocation(state.location);
                frame.setSize(state.size);
                addWindow(frame);
                break;
            default:
        }
    }

    protected void addWindow(JInternalFrame frame)
    {
        m_desktopPane.add(frame);
        frame.setVisible(true);
    }

    void setLookAndFeel(String className)
    {
        try
        {
            UIManager.setLookAndFeel(className);
            SwingUtilities.updateComponentTreeUI(m_mainFrame);
        }
        catch (ClassNotFoundException | InstantiationException
                | IllegalAccessException | UnsupportedLookAndFeelException e)
        {
            // just ignore
        }
    }
}
